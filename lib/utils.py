import json
import os
import shutil
import ssl
import stat
import urllib.error
import urllib.parse
import urllib.request

import archinstall

BASE_AUR_URL = 'https://aur.archlinux.org/rpc/?v=5&type=search&arg={package}'


def find_package(name):
    """
    Finds a specific package via the package database.
    It makes a simple web-request, which might be a bit slow.
    """
    ssl_context = ssl.create_default_context()
    ssl_context.check_hostname = False
    ssl_context.verify_mode = ssl.CERT_NONE
    response = urllib.request.urlopen(
        BASE_AUR_URL.format(package=name), context=ssl_context)
    data = json.loads(response.read().decode('UTF-8'))
    for result in data['results']:
        if result['Name'] == name:
            return True
    return False


def validate_aur_package_list(packages: list) -> list:
    """
    Validates a list of given packages.
    Raises `RequirementError` if one or more packages are not found.
    """
    valid_packages = []
    for package in packages:

        if find_package(package):
            valid_packages.append(package)
        else:
            archinstall.log(f'Package not found: {package}', fg='red')
    return valid_packages


def enable_multilib(installation: archinstall.Installer):
    replace_in_file(installation, '/etc/pacman.conf',
                    '#[multilib]\n#Include = /etc/pacman.d/mirrorlist', '[multilib]\nInclude = /etc/pacman.d/mirrorlist')
    # Update mirrors to reflext new repository
    arch_chroot(installation, 'pacman -Sy')


def add_finger_info(installation: archinstall.Installer, user: str, name: str):
    replace_in_file(installation, '/etc/passwd',
                    f'::/home/{user}', f':{name}:/home/{user}')


def set_makeflags(installation: archinstall.Installer, makeflags='-j$(nproc)'):
    replace_in_file(installation, '/etc/makepkg.conf',
                    '#MAKEFLAGS="-j2"', f'MAKEFLAGS="{makeflags}"')


def replace_in_file(installation: archinstall.Installer, path: str, before: str, after: str):
    with open(f'{installation.target}{path}', 'r') as file:
        filedata = file.read()

    filedata = filedata.replace(before, after)

    with open(f'{installation.target}{path}', 'w') as file:
        file.write(filedata)


def install_aur_helper(helper_name: str, installation: archinstall.Installer):
    installation.log(f"Installing {helper_name}...")
    user = list(archinstall.arguments.get('superusers', {}).keys())[0]
    installation.add_additional_packages(['git'])
    arch_chroot(
        installation, f'git clone https://aur.archlinux.org/{helper_name}.git /home/{user}/{helper_name}', runas=user)
    arch_chroot(
        installation, f'cd /home/{user}/{helper_name} && makepkg -si --noconfirm', runas=user)
    shutil.rmtree(f'{installation.target}/home/{user}/{helper_name}')
    arch_chroot(
        installation, f'/usr/bin/{helper_name} --save --nocleanmenu --nodiffmenu --noeditmenu --removemake', runas=user)


def arch_chroot(installation, cmd, *args, **kwargs):
    if 'runas' in kwargs:
        cmd = f"su - {kwargs['runas']} -c \"{cmd}\""

    return archinstall.SysCommand(f'/usr/bin/arch-chroot {installation.target} {cmd}', peak_output=True)


def install_aur_packages(installation: archinstall.Installer, *packages, **kwargs):
    if type(packages[0]) in (list, tuple):
        packages = packages[0]
    installation.log(
        f'Installing packages: {packages}', level=archinstall.logging.INFO)

    user = list(archinstall.arguments.get('superusers', {}).keys())[0]

    if (sync_mirrors := archinstall.SysCommand('/usr/bin/pacman -Syy')).exit_code == 0:
        for package in packages:
            installation.log(
                f'Installing package: {package}', level=archinstall.logging.INFO)
            if (pacstrap := arch_chroot(installation, f'/usr/bin/{archinstall.arguments["aur-helper"]} -S --noconfirm {package}', runas=user)).exit_code == 0:
                installation.log(
                    f'Installed {package}', level=archinstall.logging.INFO)
            else:
                installation.log(
                    f'Could not install packages: {pacstrap.exit_code}', level=archinstall.logging.INFO)
    else:
        installation.log(
            f'Could not sync mirrors: {sync_mirrors.exit_code}', level=archinstall.logging.INFO)


def install_dconf(installation: archinstall.Installer, dconf_path: str):
    user = list(archinstall.arguments.get('superusers', {}).keys())[0]

    shutil.copy(dconf_path, f'{installation.target}/home/{user}/dconf-backup')

    # Create script to restore dconf on first login, then self-destruct
    with open(f'{installation.target}/home/{user}/restore-dconf.sh', 'w') as file:
        file.write(f"""#!/bin/sh
/usr/bin/dconf load / < /home/{user}/dconf-backup
rm /home/{user}/dconf-backup
rm /home/{user}/.config/autostart/dconf.desktop
rm /home/{user}/restore-dconf.sh""")

    # Create directory if it doesn't exist
    os.makedirs(
        f'{installation.target}/home/{user}/.config/autostart', exist_ok=True)

    # Create XDG auto-launch entry
    with open(f'{installation.target}/home/{user}/.config/autostart/dconf.desktop', 'w') as file:
        file.write(f"""[Desktop Entry]
Name=Restore dconf
Exec=/home/{user}/restore-dconf.sh
Terminal=false
Type=Application
X-GNOME-Autostart-enabled=true""")

    # Make everything executable by all users
    os.chmod(f'{installation.target}/home/{user}/dconf-backup',
             stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO)
    os.chmod(f'{installation.target}/home/{user}/restore-dconf.sh',
             stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO)
    os.chmod(f'{installation.target}/home/{user}/.config/autostart/dconf.desktop',
             stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO)


def dconf_to_gsettings(dconf_path: str):
    with open(f'{dconf_path}', 'r') as file:
        filedata = file.read()

    filedata = filedata.split('\n\n')

    for config in filedata:
        title, options = config.split('\n')[0], config.split('\n')[1:]
        title = title.strip('[]').replace('/', '.')
        for option in options:
            option = option.replace('=', ' ')
            if len(option):
                print(f'{title} {option}')


def apply_gsettings(installation: archinstall.Installer, gsettings_path: str):
    with open(gsettings_path, 'r') as file:
        filedata = file.read()

    filedata = filedata.strip('\n').split('\n')

    user = list(archinstall.arguments.get('superusers', {}).keys())[0]

    for config in filedata:
        arch_chroot(
            installation, f'/usr/bin/gsettings set {config}', runas=user)
