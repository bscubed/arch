import json
import logging
import os
import time

import archinstall
from archinstall.lib.general import run_custom_user_commands
from archinstall.lib.hardware import AVAILABLE_GFX_DRIVERS, has_uefi
from archinstall.lib.networking import check_mirror_reachable
from archinstall.lib.profiles import Profile
from lib.utils import *

if archinstall.arguments.get('help'):
    print("See `man archinstall` for help.")
    exit(0)
if os.getuid() != 0:
    print("Archinstall requires root privileges to run. See --help for more.")
    exit(1)

# For support reasons, we'll log the disk layout pre installation to match against post-installation layout
archinstall.log(
    f"Disk states before installing: {archinstall.disk_layouts()}", level=logging.DEBUG)


def ask_user_questions():
    """
            First, we'll ask the user for a bunch of user input.
            Not until we're satisfied with what we want to install
            will we continue with the actual installation steps.
    """
    if not archinstall.arguments.get('keyboard-language', None):
        while True:
            try:
                archinstall.arguments['keyboard-language'] = archinstall.select_language(
                    archinstall.list_keyboard_languages()).strip()
                break
            except archinstall.RequirementError as err:
                archinstall.log(err, fg="red")

    # Before continuing, set the preferred keyboard layout/language in the current terminal.
    # This will just help the user with the next following questions.
    if len(archinstall.arguments['keyboard-language']):
        archinstall.set_keyboard_language(
            archinstall.arguments['keyboard-language'])

    # Set which region to download packages from during the installation
    if not archinstall.arguments.get('mirror-region', None):
        while True:
            try:
                archinstall.arguments['mirror-region'] = archinstall.select_mirror_regions(
                    archinstall.list_mirrors())
                break
            except archinstall.RequirementError as e:
                archinstall.log(e, fg="red")
    else:
        selected_region = archinstall.arguments['mirror-region']
        archinstall.arguments['mirror-region'] = {
            selected_region: archinstall.list_mirrors()[selected_region]}

    if not archinstall.arguments.get('sys-language', None) and archinstall.arguments.get('advanced', False):
        archinstall.arguments['sys-language'] = input(
            "Enter a valid locale (language) for your OS, (Default: en_US): ").strip()
        archinstall.arguments['sys-encoding'] = input(
            "Enter a valid system default encoding for your OS, (Default: utf-8): ").strip()
        archinstall.log(
            "Keep in mind that if you want multiple locales, post configuration is required.", fg="yellow")

    if not archinstall.arguments.get('sys-language', None):
        archinstall.arguments['sys-language'] = 'en_US'
    if not archinstall.arguments.get('sys-encoding', None):
        archinstall.arguments['sys-encoding'] = 'utf-8'

    # Ask which harddrive/block-device we will install to
    if archinstall.arguments.get('harddrive', None):
        archinstall.arguments['harddrive'] = archinstall.BlockDevice(
            archinstall.arguments['harddrive'])
    else:
        archinstall.arguments['harddrive'] = archinstall.select_disk(
            archinstall.all_disks())
        if archinstall.arguments['harddrive'] is None:
            archinstall.arguments['target-mount'] = archinstall.storage.get(
                'MOUNT_POINT', '/mnt')

    # Perform a quick sanity check on the selected harddrive.
    # 1. Check if it has partitions
    # 3. Check that we support the current partitions
    # 2. If so, ask if we should keep them or wipe everything
    if archinstall.arguments['harddrive'] and archinstall.arguments['harddrive'].has_partitions():
        archinstall.log(
            f"{archinstall.arguments['harddrive']} contains the following partitions:", fg='yellow')

        # We curate a list pf supported partitions
        # and print those that we don't support.
        partition_mountpoints = {}
        for partition in archinstall.arguments['harddrive']:
            try:
                if partition.filesystem_supported():
                    archinstall.log(f" {partition}")
                    partition_mountpoints[partition] = None
            except archinstall.UnknownFilesystemFormat as err:
                archinstall.log(
                    f" {partition} (Filesystem not supported)", fg='red')

        # We then ask what to do with the partitions.
        if (option := 'format-all' if archinstall.arguments.get('format', False) else archinstall.ask_for_disk_layout()) == 'abort':
            archinstall.log(
                "Safely aborting the installation. No changes to the disk or system has been made.")
            exit(1)
        elif option == 'keep-existing':
            archinstall.arguments['harddrive'].keep_partitions = True

            archinstall.log(
                " ** You will now select which partitions to use by selecting mount points (inside the installation). **")
            archinstall.log(
                " ** The root would be a simple / and the boot partition /boot (as all paths are relative inside the installation). **")
            mountpoints_set = []
            while True:
                # Select a partition
                # If we provide keys as options, it's better to convert them to list and sort before passing
                mountpoints_list = sorted(list(partition_mountpoints.keys()))
                partition = archinstall.generic_select(
                    mountpoints_list, "Select a partition by number that you want to set a mount-point for (leave blank when done): ")
                if not partition:
                    if set(mountpoints_set) & {'/', '/boot'} == {'/', '/boot'}:
                        break

                    continue

                # Select a mount-point
                mountpoint = input(
                    f"Enter a mount-point for {partition}: ").strip(' ')
                if len(mountpoint):

                    # Get a valid & supported filesystem for the partition:
                    while 1:
                        new_filesystem = input(
                            f"Enter a valid filesystem for {partition} (leave blank for {partition.filesystem}): ").strip(' ')
                        if len(new_filesystem) <= 0:
                            if partition.encrypted and partition.filesystem == 'crypto_LUKS':
                                old_password = archinstall.arguments.get(
                                    '!encryption-password', None)
                                if not old_password:
                                    old_password = input(
                                        f'Enter the old encryption password for {partition}: ')

                                if autodetected_filesystem := partition.detect_inner_filesystem(old_password):
                                    new_filesystem = autodetected_filesystem
                                else:
                                    archinstall.log(
                                        "Could not auto-detect the filesystem inside the encrypted volume.", fg='red')
                                    archinstall.log(
                                        "A filesystem must be defined for the unlocked encrypted partition.")
                                    continue
                            break

                        # Since the potentially new filesystem is new
                        # we have to check if we support it. We can do this by formatting /dev/null with the partitions filesystem.
                        # There's a nice wrapper for this on the partition object itself that supports a path-override during .format()
                        try:
                            partition.format(new_filesystem, path='/dev/null',
                                             log_formatting=False, allow_formatting=True)
                        except archinstall.UnknownFilesystemFormat:
                            archinstall.log(
                                f"Selected filesystem is not supported yet. If you want archinstall to support '{new_filesystem}',")
                            archinstall.log(
                                "please create a issue-ticket suggesting it on github at https://github.com/archlinux/archinstall/issues.")
                            archinstall.log(
                                "Until then, please enter another supported filesystem.")
                            continue
                        except archinstall.SysCallError:
                            # Expected exception since mkfs.<format> can not format /dev/null. But that means our .format() function supported it.
                            pass
                        break

                    # When we've selected all three criteria,
                    # We can safely mark the partition for formatting and where to mount it.
                    # TODO: allow_formatting might be redundant since target_mountpoint should only be
                    #       set if we actually want to format it anyway.
                    mountpoints_set.append(mountpoint)
                    partition.allow_formatting = True
                    partition.target_mountpoint = mountpoint
                    # Only overwrite the filesystem definition if we selected one:
                    if len(new_filesystem):
                        partition.filesystem = new_filesystem

            archinstall.log('Using existing partition table reported above.')
        elif option == 'format-all':
            if not archinstall.arguments.get('filesystem', None):
                archinstall.arguments['filesystem'] = archinstall.ask_for_main_filesystem_format(
                )
            archinstall.arguments['harddrive'].keep_partitions = False
    elif archinstall.arguments['harddrive']:
        # If the drive doesn't have any partitions, safely mark the disk with keep_partitions = False
        # and ask the user for a root filesystem.
        if not archinstall.arguments.get('filesystem', None):
            archinstall.arguments['filesystem'] = archinstall.ask_for_main_filesystem_format(
            )
        archinstall.arguments['harddrive'].keep_partitions = False

    # Get disk encryption password (or skip if blank)
    if archinstall.arguments['harddrive'] and archinstall.arguments.get('!encryption-password', None) == '':
        archinstall.arguments['!encryption-password'] = None
        archinstall.arguments['harddrive'].encryption_password = None
    elif archinstall.arguments['harddrive'] and archinstall.arguments.get('!encryption-password', None) is None:
        if passwd := archinstall.get_password(prompt='Enter disk encryption password (leave blank for no encryption): '):
            archinstall.arguments['!encryption-password'] = passwd
            archinstall.arguments['harddrive'].encryption_password = archinstall.arguments['!encryption-password']
    # Ask for which bootloader to use
    if not archinstall.arguments.get('bootloader', None):
        archinstall.arguments['bootloader'] = archinstall.ask_for_bootloader()
    # Get the hostname for the machine
    if not archinstall.arguments.get('hostname', None):
        archinstall.arguments['hostname'] = input(
            'Desired hostname for the installation: ').strip(' ')
    # Ask for a root password (optional, but triggers requirement for super-user if skipped)
    if not archinstall.arguments.get('!root-password', None):
        if archinstall.arguments['!root-password'] == '':
            archinstall.arguments['!root-password'] = None
        else:
            archinstall.arguments['!root-password'] = archinstall.get_password(
                prompt='Enter root password (Recommendation: leave blank to leave root disabled): ')

    # Ask for additional users (super-user if root pw was not set)
    users = {}
    superusers = {}

    if archinstall.arguments.get('users', None):
        for user, value in archinstall.arguments['users'].items():
            if value['!password'] is None:
                value['!password'] = archinstall.get_password(
                    prompt=f'Password for user {user}: ')
            users[user] = value
    archinstall.arguments['users'] = users

    if archinstall.arguments.get('superusers', None):
        for superuser, value in archinstall.arguments['superusers'].items():
            if value['!password'] is None:
                value['!password'] = archinstall.get_password(
                    prompt=f'Password for superuser {superuser}: ')
            superusers[superuser] = value
    archinstall.arguments['superusers'] = superusers

    if not archinstall.arguments.get('!root-password', None) and not archinstall.arguments['superusers']:
        archinstall.arguments['superusers'] = archinstall.ask_for_superuser_account(
            'Create a required super-user with sudo privileges: ', forced=True)

    # Ask for archinstall-specific profiles (such as desktop environments etc)
    if not archinstall.arguments.get('profile', None):
        archinstall.arguments['profile'] = archinstall.select_profile()
    else:
        archinstall.arguments['profile'] = Profile(
            installer=None, path=archinstall.arguments['profile'])

    # Check the potentially selected profiles preparations to get early checks if some additional questions are needed.
    if archinstall.arguments['profile'] and archinstall.arguments['profile'].has_prep_function():
        with archinstall.arguments['profile'].load_instructions(namespace=f"{archinstall.arguments['profile'].namespace}.py") as imported:
            if not imported._prep_function():
                archinstall.log(
                    ' * Profile\'s preparation requirements was not fulfilled.', fg='red')
                exit(1)

    # Ask about audio server selection if one is not already set
    if not archinstall.arguments.get('audio', None):
        # only ask for audio server selection on a desktop profile
        if str(archinstall.arguments['profile']) == 'Profile(desktop)':
            archinstall.arguments['audio'] = archinstall.ask_for_audio_selection(
            )
        else:
            # packages installed by a profile may depend on audio and something may get installed anyways, not much we can do about that.
            # we will not try to remove packages post-installation to not have audio, as that may cause multiple issues
            archinstall.arguments['audio'] = None

    # Ask for preferred kernel:
    if not archinstall.arguments.get("kernels", None):
        kernels = ["linux", "linux-lts", "linux-zen", "linux-hardened"]
        archinstall.arguments['kernels'] = archinstall.select_kernel(kernels)

    # Additional packages (with some light weight error handling for invalid package names)
    while True:
        if not archinstall.arguments.get('packages', None):
            archinstall.arguments['packages'] = [package for package in input(
                'Write additional packages to install (space separated, leave blank to skip): ').split(' ') if len(package)]

        if len(archinstall.arguments['packages']):
            # Verify packages that were given
            try:
                archinstall.log(
                    "Verifying that additional packages exist (this might take a few seconds)")
                archinstall.validate_package_list(
                    archinstall.arguments['packages'])
                break
            except archinstall.RequirementError as e:
                archinstall.log(e, fg='red')
                # Clear the packages to trigger a new input question
                archinstall.arguments['packages'] = None
        else:
            # no additional packages were selected, which we'll allow
            break

    # Additional AUR packages (with some light weight error handling for invalid package names)
    while True:
        if not archinstall.arguments.get('aur-packages', None):
            archinstall.arguments['aur-packages'] = [package for package in input(
                'Write additional packages to install (space separated, leave blank to skip): ').split(' ') if len(package)]

        if len(archinstall.arguments['aur-packages']):
            # Verify packages that were given
            archinstall.log(
                "Verifying that additional AUR packages exist (this might take a few seconds)")
            archinstall.arguments['aur-packages'] = validate_aur_package_list(
                archinstall.arguments['aur-packages'])
            if len(archinstall.arguments['aur-packages']):
                break
            else:
                # Clear the packages to trigger a new input question
                archinstall.arguments['aur-packages'] = None
        else:
            # no additional packages were selected, which we'll allow
            break

    # Ask or Call the helper function that asks the user to optionally configure a network.
    if not archinstall.arguments.get('nic', None):
        archinstall.arguments['nic'] = archinstall.ask_to_configure_network()
        if not archinstall.arguments['nic']:
            archinstall.log(
                "No network configuration was selected. Network is going to be unavailable until configured manually!", fg="yellow")

    if not archinstall.arguments.get('timezone', None):
        archinstall.arguments['timezone'] = archinstall.ask_for_a_timezone()

    if archinstall.arguments['timezone']:
        if not archinstall.arguments.get('ntp', False):
            archinstall.arguments['ntp'] = input(
                "Would you like to use automatic time synchronization (NTP) with the default time servers? [Y/n]: ").strip().lower() in ('y', 'yes', '')
            if archinstall.arguments['ntp']:
                archinstall.log(
                    "Hardware time and other post-configuration steps might be required in order for NTP to work. For more information, please check the Arch wiki.", fg="yellow")


def perform_installation_steps():
    if not archinstall.arguments.get('silent'):
        print()
        print('This is your chosen configuration:')
        archinstall.log(
            "-- Guided template chosen (with below config) --", level=logging.DEBUG)
        user_configuration = json.dumps(
            archinstall.arguments, indent=4, sort_keys=True, cls=archinstall.JSON)
        archinstall.log(user_configuration, level=logging.INFO)
        with open("/var/log/archinstall/user_configuration.json", "w") as config_file:
            config_file.write(user_configuration)
        print()
        input('Press Enter to continue.')

    """
		Issue a final warning before we continue with something un-revertable.
		We mention the drive one last time, and count from 5 to 0.
	"""

    if archinstall.arguments.get('harddrive', None):
        print(
            f" ! Formatting {archinstall.arguments['harddrive']} in ", end='')
        archinstall.do_countdown()

        """
			Setup the blockdevice, filesystem (and optionally encryption).
			Once that's done, we'll hand over to perform_installation()
		"""
        mode = archinstall.GPT
        if has_uefi() is False:
            mode = archinstall.MBR
        with archinstall.Filesystem(archinstall.arguments['harddrive'], mode) as fs:
            # Wipe the entire drive if the disk flag `keep_partitions`is False.
            if archinstall.arguments['harddrive'].keep_partitions is False:
                fs.use_entire_disk(
                    root_filesystem_type=archinstall.arguments.get('filesystem', 'btrfs'))

            # Check if encryption is desired and mark the root partition as encrypted.
            if archinstall.arguments.get('!encryption-password', None):
                root_partition = fs.find_partition('/')
                root_partition.encrypted = True

            # After the disk is ready, iterate the partitions and check
            # which ones are safe to format, and format those.
            for partition in archinstall.arguments['harddrive']:
                if partition.safe_to_format():
                    # Partition might be marked as encrypted due to the filesystem type crypt_LUKS
                    # But we might have omitted the encryption password question to skip encryption.
                    # In which case partition.encrypted will be true, but passwd will be false.
                    if partition.encrypted and (passwd := archinstall.arguments.get('!encryption-password', None)):
                        partition.encrypt(password=passwd)
                    else:
                        partition.format()
                else:
                    archinstall.log(
                        f"Did not format {partition} because .safe_to_format() returned False or .allow_formatting was False.", level=logging.DEBUG)

            if archinstall.arguments.get('!encryption-password', None):
                # First encrypt and unlock, then format the desired partition inside the encrypted part.
                # archinstall.luks2() encrypts the partition when entering the with context manager, and
                # unlocks the drive so that it can be used as a normal block-device within archinstall.
                with archinstall.luks2(fs.find_partition('/'), 'luksloop', archinstall.arguments.get('!encryption-password', None)) as unlocked_device:
                    unlocked_device.format(fs.find_partition('/').filesystem)
                    unlocked_device.mount(
                        archinstall.storage.get('MOUNT_POINT', '/mnt'))
            else:
                fs.find_partition(
                    '/').mount(archinstall.storage.get('MOUNT_POINT', '/mnt'))

            if has_uefi():
                fs.find_partition(
                    '/boot').mount(archinstall.storage.get('MOUNT_POINT', '/mnt') + '/boot')

    perform_installation(archinstall.storage.get('MOUNT_POINT', '/mnt'))


def perform_installation(mountpoint):
    """
    Performs the installation steps on a block device.
    Only requirement is that the block devices are
    formatted and setup prior to entering this function.
    """
    with archinstall.Installer(mountpoint, kernels=archinstall.arguments.get('kernels', 'linux')) as installation:
        # Certain services might be running that affects the system during installation.
        # Currently, only one such service is "reflector.service" which updates /etc/pacman.d/mirrorlist
        # We need to wait for it before we continue since we opted in to use a custom mirror/region.
        installation.log(
            'Waiting for automatic mirror selection (reflector) to complete.', level=logging.INFO)
        while archinstall.service_state('reflector') not in ('dead', 'failed'):
            time.sleep(1)
        # Set mirrors used by pacstrap (outside of installation)
        if archinstall.arguments.get('mirror-region', None):
            # Set the mirrors for the live medium
            archinstall.use_mirrors(archinstall.arguments['mirror-region'])
        if installation.minimal_installation():
            installation.set_locale(
                archinstall.arguments['sys-language'], archinstall.arguments['sys-encoding'].upper())
            installation.set_hostname(archinstall.arguments['hostname'])
            if archinstall.arguments['mirror-region'].get("mirrors", None) is not None:
                # Set the mirrors in the installation medium
                installation.set_mirrors(
                    archinstall.arguments['mirror-region'])
            if archinstall.arguments["bootloader"] == "grub-install" and has_uefi():
                installation.add_additional_packages("grub")
            installation.add_bootloader(archinstall.arguments["bootloader"])

            # If user selected to copy the current ISO network configuration
            # Perform a copy of the config
            if archinstall.arguments.get('nic', {}) == 'Copy ISO network configuration to installation':
                # Sources the ISO network configuration to the install medium.
                installation.copy_iso_network_config(enable_services=True)
            elif archinstall.arguments.get('nic', {}).get('NetworkManager', False):
                installation.add_additional_packages("networkmanager")
                installation.enable_service('NetworkManager.service')
            # Otherwise, if a interface was selected, configure that interface
            elif archinstall.arguments.get('nic', {}):
                installation.configure_nic(
                    **archinstall.arguments.get('nic', {}))
                installation.enable_service('systemd-networkd')
                installation.enable_service('systemd-resolved')

            if archinstall.arguments.get('audio', None) is not None:
                installation.log(
                    f"This audio server will be used: {archinstall.arguments.get('audio', None)}", level=logging.INFO)
                if archinstall.arguments.get('audio', None) == 'pipewire':
                    print('Installing pipewire ...')

                    installation.add_additional_packages(["pipewire", "pipewire-alsa", "pipewire-jack",
                                                         "pipewire-media-session", "pipewire-pulse", "gst-plugin-pipewire", "libpulse"])
                elif archinstall.arguments.get('audio', None) == 'pulseaudio':
                    print('Installing pulseaudio ...')
                    installation.add_additional_packages("pulseaudio")
            else:
                installation.log(
                    "No audio server will be installed.", level=logging.INFO)

            # Enabling multilib repository
            enable_multilib(installation)

            # Enabling pacman color
            replace_in_file(installation, '/etc/pacman.conf',
                            '#Color', 'Color')

            # Enabling pacman parallel downloads
            replace_in_file(installation, '/etc/pacman.conf',
                            '#ParallelDownloads = 5', 'ParallelDownloads = 5')

            # Increasing makeflags in makepkg.conf
            set_makeflags(installation)

            if archinstall.arguments.get('packages', None) and archinstall.arguments.get('packages', None)[0] != '':
                installation.add_additional_packages(
                    archinstall.arguments.get('packages', None))

            if archinstall.arguments.get('profile', None):
                installation.install_profile(
                    archinstall.arguments.get('profile', None))

            for user, user_info in archinstall.arguments.get('users', {}).items():
                installation.user_create(
                    user, user_info["!password"], sudo=False)
                if user_info["name"]:
                    add_finger_info(installation, user, user_info["name"])

            for superuser, user_info in archinstall.arguments.get('superusers', {}).items():
                installation.user_create(
                    superuser, user_info["!password"], sudo=True)
                with open(f'{installation.target}/etc/sudoers', 'a') as sudoers:
                    sudoers.write(f'{superuser} ALL=(ALL) NOPASSWD: ALL\n')
                if user_info["name"]:
                    add_finger_info(installation, superuser, user_info["name"])
                installation.helper_flags['user'] = True

            if timezone := archinstall.arguments.get('timezone', None):
                installation.set_timezone(timezone)

            if archinstall.arguments.get('ntp', False):
                installation.activate_ntp()

            if (root_pw := archinstall.arguments.get('!root-password', None)) and len(root_pw):
                installation.user_set_pw('root', root_pw)

            # This step must be after profile installs to allow profiles to install language pre-requisits.
            # After which, this step will set the language both for console and x11 if x11 was installed for instance.
            installation.set_keyboard_language(
                archinstall.arguments['keyboard-language'])

            if archinstall.arguments['profile'] and archinstall.arguments['profile'].has_post_install():
                with archinstall.arguments['profile'].load_instructions(namespace=f"{archinstall.arguments['profile'].namespace}.py") as imported:
                    if not imported._post_install():
                        archinstall.log(
                            ' * Profile\'s post configuration requirements was not fulfilled.', fg='red')
                        exit(1)

        # If the user provided a list of services to be enabled, pass the list to the enable_service function.
        # Note that while it's called enable_service, it can actually take a list of services and iterate it.
        if archinstall.arguments.get('services', None):
            installation.enable_service(*archinstall.arguments['services'])

        # Display warning message when no AUR helper specified.
        if archinstall.arguments.get('aur-packages', None) and not archinstall.arguments.get('aur-helper', None):
            installation.log(
                f"No AUR helper specified. No AUR packages will be installed. Add 'aur-helper' to the config")

        # If the user provided an AUR helper to be installed, install it now.
        # In addition, install user-defined AUR packages, if they exist.
        if archinstall.arguments.get('aur-helper', None):
            install_aur_helper(
                archinstall.arguments['aur-helper'], installation)
            if archinstall.arguments.get('aur-packages', None):
                install_aur_packages(
                    installation, archinstall.arguments['aur-packages'])

        # If user specified a dconf backup, install it to the primary user now
        if archinstall.arguments.get('dconf', None):
            install_dconf(installation, archinstall.arguments['dconf'])

        # If the user provided custom commands to be run post-installation, execute them now.
        if archinstall.arguments.get('custom-commands', None):
            run_custom_user_commands(
                archinstall.arguments['custom-commands'], installation)

        installation.log(
            "For post-installation tips, see https://wiki.archlinux.org/index.php/Installation_guide#Post-installation", fg="yellow")

        choice = input(
            "Would you like to chroot into the newly created installation and perform post-installation configuration? [Y/n] ")
        if choice.lower() in ("y", ""):
            try:
                installation.drop_to_shell()
            except:
                pass

    # For support reasons, we'll log the disk layout post installation (crash or no crash)
    archinstall.log(
        f"Disk states after installing: {archinstall.disk_layouts()}", level=logging.DEBUG)


if not check_mirror_reachable():
    log_file = os.path.join(archinstall.storage.get(
        'LOG_PATH', None), archinstall.storage.get('LOG_FILE', None))
    archinstall.log(
        f"Arch Linux mirrors are not reachable. Please check your internet connection and the log file '{log_file}'.", level=logging.INFO, fg="red")
    exit(1)

ask_user_questions()

perform_installation_steps()
